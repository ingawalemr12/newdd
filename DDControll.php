<?php
class Location extends CI_Controller
{
	public function index()
	{
		$this->load->view('include/header');
		$this->load->model('Location_model');
		$data['data'] = $this->Location_model->getdata();
		$data['regions'] = $this->Location_model->get_region();
		$data['states'] = $this->Location_model->get_states();
		$data['city'] = $this->Location_model->get_cities();
		//print_r($data);
		$this->load->view('location_view',$data);
	}

	public function add_country()
	{
		$data = array('country_name'=>$this->input->post('country_name'));
		 // print_r($data);
		$this->load->model('Location_model');
		$result=$this->Location_model->add_country($data);
		if($result)
		{
			redirect('Location');
		}
	}
	
	public function add_region()
	{
		$this->load->model('Location_model');
		$formArray=array();
		$formArray['country_id'] = $this->input->post('country_name');
		$formArray['region_name'] = $this->input->post('region_name');
		$this->Location_model->add_region($formArray);
		redirect('Location');
	}
	
	public function get_regions()	// dropdown menu-fetch_region
	{
		$this->load->model('Location_model');
		$country_id = $this->input->post('country_id');
		echo $this->Location_model->fetch_region($country_id);
	}

	public function add_state()
	{
		$this->load->model('Location_model');
		$formArray=array();
		$formArray['country_id'] = $this->input->post('countries');
		$formArray['region_id'] = $this->input->post('region');
		$formArray['state_name'] = $this->input->post('state_name');

		$this->Location_model->add_state($formArray);
		redirect('Location');
	}
	
	public function get_states()	// dropdown menu-get_states
	{
		$this->load->model('Location_model');
		$region_id = $this->input->post('region_id');
		echo $this->Location_model->fetch_states($region_id);
	}

	public function add_city()
	{
		$this->load->model('Location_model');
		$formArray=array();
		$formArray['city_name'] = $this->input->post('city_name');
		$formArray['state_id'] = $this->input->post('state');
		$formArray['country_id'] = $this->input->post('country');
		$formArray['region_id'] = $this->input->post('regions');

		$this->Location_model->add_city($formArray);
		redirect('Location');
	}

}
?>