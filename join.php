Database: examplestudy »Table: student

Roll_No 	Name 	Gender 	Mob_No

   3 		anita 	female	9675432890
   4 		ankita 	female 	9876543210
   5 		mahima 	female 	8976453201

Database: examplestudy »Table: employee

id 		name 		salary

1 		mahadev		21800
2 		john 		21800
3 		Pankaja		23000
4 		Rakshit		30000
5 		Rahul		10000
6 		Rahul		25000

//....................................LEFT JOIN
<?php
"SELECT student.Name,employee.salary FROM student LEFT JOIN employee on student.Roll_No=employee.id";

		Name	salary
		anita  	23000
		ankita 	30000
		mahima 	10000

"SELECT student.Name,employee.salary FROM employee LEFT JOIN student on student.Roll_No=employee.id"

Name 	salary
NULL 	21800
NULL 	21800
anita 	23000
ankita 	30000
mahima 	10000
NULL 	25000


//....................................RIGHT JOIN

"SELECT student.Name,employee.salary FROM student RIGHT JOIN employee on student.Roll_No=employee.id"

Name 	salary
NULL 	21800
NULL 	21800
anita 	23000
ankita 	30000
mahima 	10000
NULL 	25000

"SELECT student.Name,employee.salary FROM employee RIGHT JOIN student on student.Roll_No=employee.id"

		Name	salary
		anita  	23000
		ankita 	30000
		mahima 	10000
?>