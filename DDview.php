

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>AdminLTE 3 | DataTables</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php //echo base_url();?>plugins/fontawesome-free/css/all.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url();?>plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>dist/css/adminlte.min.css">
  <!--
</head>

 -->

<!--employee master view added -->

 <div class="col-sm-6">
            <button type="button" class="btn btn-secondary"><a href="Location_master" style="color: white;">Create Location</a></button>
          </div>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
<!--             <div class="card">
 -->          
              
          
            <div class="card">
              <!-- <div class="card-header">
                
              </div> -->
              <!-- /.card-header -->
              <div class="card-body">
                  <div class="row">
                    <div class="col-12 col-sm-12">
                      <div class="card card-primary card-tabs">
                        <div class="card-header p-0 pt-1">
                          <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                            <li class="nav-item">
                              <a class="nav-link active" id="custom-tabs-one-home-tab" data-toggle="pill" href="#custom-tabs-one-home" role="tab" aria-controls="custom-tabs-one-home" aria-selected="true">Country</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" id="custom-tabs-one-profile-tab" data-toggle="pill" href="#custom-tabs-one-profile" role="tab" aria-controls="custom-tabs-one-profile" aria-selected="false">Region</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" id="custom-tabs-one-messages-tab" data-toggle="pill" href="#custom-tabs-one-messages" role="tab" aria-controls="custom-tabs-one-messages" aria-selected="false">State </a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" id="custom-tabs-one-settings-tab" data-toggle="pill" href="#custom-tabs-one-settings" role="tab" aria-controls="custom-tabs-one-settings" aria-selected="false">City </a>
                            </li>
                          </ul>
                        </div>

                        <div class="card-body">
                          <div class="tab-content" id="custom-tabs-one-tabContent">

                            <!-- Country Tab -->
                            <div class="tab-pane fade show active" id="custom-tabs-one-home" role="tabpanel" aria-labelledby="custom-tabs-one-home-tab">
                                <section class="content">
                                  <div class="container-fluid">
                                    <div class="row">
                                      <!-- left column -->
                                      <div class="col-md-6">
                                        <!-- general form elements -->
                                        <div class="card card-secondary">
                                          <div class="card-header">
                                            <h3 class="card-title">Country Master</h3>
                                          </div>
                                          <!-- form start -->
                                          <form action="<?php echo base_url('Location/add_country') ?>" method="POST">
                                            <div class="card-body">
                                              <div class="form-group">
                                                <label>Country</label>
                                                <input type="text" name="country_name" id="country_name" class="form-control" placeholder="Country Name" required>
                                              </div>
                                            </div>
                                            <div class="card-footer">
                                              <button type="submit" class="btn btn-primary">Submit</button>
                                            </div>
                                          </form>
                                        </div>
                                        <!-- /.card -->  

                                      </div>

                                      <div class="col-md-6">
                                        <div class="card card-secondary">
                                          <div class="card-header">
                                            <h3 class="card-title">Country Table</h3>
                                          </div>
                                          <div class="card-body">
                                            <table class="table table-bordered">
                                              <thead>
                                                <tr>
                                                  <th>Sr. No</th>
                                                  <th>Country</th>
                                                </tr>
                                              </thead>
                                              <?php 
                                              if (!empty($data)) {
                                              foreach($data as $row) { ?>
                                                <tbody>
                                                <tr>
                                                  <td><?php echo $row['country_id'];?></td>
                                                  <td><?php echo $row['country_name'];?></td>
                                                </tr>
                                                </tbody>
                                                <?php  } } ?>
                                            </table>
                                          </div>
                                        </div>
                                      </div>

                                    </div>
                                  </div>
                                </section>
                            </div>

                            <!-- Region Tab -->
                            <div class="tab-pane fade" id="custom-tabs-one-profile" role="tabpanel" aria-labelledby="custom-tabs-one-profile-tab">
                                <section class="content">
                                  <div class="container-fluid">
                                    <div class="row">
                                      <!-- left column -->
                                      <div class="col-md-6">
                                        <!-- general form elements -->
                                        <div class="card card-secondary">
                                          <div class="card-header">
                                            <h3 class="card-title">Region Master</h3>
                                          </div>
                                          <!-- form start -->
                                          <form action="<?php echo base_url('Location/add_region') ?>" method="POST">
                                            
                                            <div class="card-body">
                                              <div class="form-group">
                                                <label>Country</label>
                                                <select class="form-control" name="country_name" id="country_name">
                                                  <option value="">Select a Country</option>
                                                   
                                                   <?php 
                                                    if (!empty($data)) {
                                                    foreach($data as $row) { ?>
                                                      
                                                    <option value="<?php echo $row['country_id'];?>">
                                                      <?php echo $row['country_name'];?>
                                                    </option>
                                                    <?php  } } ?>
                                                   
                                                </select><br>

                                                <label>Region</label>
                                                <input type="text" name="region_name" id="region_name" class="form-control" placeholder="Region Name" required>
                                              </div>
                                            </div>
                                            <div class="card-footer">
                                              <button type="submit" class="btn btn-primary">Submit</button>
                                            </div>
                                          </form>
                                        </div>
                                        <!-- /.card -->  

                                      </div>

                                      <div class="col-md-6">
                                        <div class="card card-secondary">
                                          <div class="card-header">
                                            <h3 class="card-title">Region Table</h3>
                                          </div>
                                          <div class="card-body">
                                            <table class="table table-bordered">
                                              <thead>
                                                <tr>
                                                  <th>Sr. No</th>
                                                  <th>Country</th>
                                                  <th>Region</th>
                                                </tr>
                                              </thead>
                                              
                                              <?php 
                                                if (!empty($regions)) {
                                                foreach($regions as $row) { ?>
                                              <tbody>
                                                <tr>
                                                  <td><?php echo $row['region_id'];?></td>
                                                  <td><?php echo $row['country_id'];?></td>
                                                  <td><?php echo $row['region_name'];?></td>
                                                </tr>
                                              </tbody>
                                                  <?php  } } ?>  
                                            </table>
                                          </div>
                                        </div>
                                      </div>

                                    </div>
                                  </div>
                                </section>
                            </div>

                            <!-- State Tab --> 
                            <div class="tab-pane fade" id="custom-tabs-one-messages" role="tabpanel" aria-labelledby="custom-tabs-one-messages-tab">
                                <section class="content">
                                  <div class="container-fluid">
                                    <div class="row">
                                      <!-- left column -->
                                      <div class="col-md-6">
                                        <!-- general form elements -->
                                        <div class="card card-secondary">
                                          <div class="card-header">
                                            <h3 class="card-title">State Master</h3>
                                          </div>
                                          <!-- form start -->
                                          <form action="<?php echo base_url('Location/add_state') ?>" method="POST">
                                            
                                            <div class="card-body">
                                              <div class="form-group">
                                                <label>Country</label>
                                                <select class="form-control" name="countries" id="countries">
                                                  <option value="">Select a Country</option>
                                                   
                                                   <?php 
                                                    if (!empty($data)) {
                                                    foreach($data as $row) { ?>
                                                      
                                                    <option value="<?php echo $row['country_id'];?>">
                                                      <?php echo $row['country_name'];?>
                                                    </option>
                                                    <?php  } } ?>
                                                   
                                                </select><br>

                                                <label>Region</label>
                                                <select class="form-control" name="region" id="region">
                                                  <option value="">Select a Region</option>

                                                </select><br>

                                                <label>State</label>
                                                <input type="text" name="state_name" id="state_name" class="form-control" placeholder="State Name" required>
                                              </div>
                                            </div>
                                            <div class="card-footer">
                                              <button type="submit" class="btn btn-primary">Submit</button>
                                            </div>
                                          </form>
                                        </div>
                                        <!-- /.card -->  

                                      </div>

                                      <div class="col-md-6">
                                        <div class="card card-secondary">
                                          <div class="card-header">
                                            <h3 class="card-title">State Table</h3>
                                          </div>
                                          <div class="card-body">
                                            <table class="table table-bordered">
                                              <thead>
                                                <tr>
                                                  <th>Sr. No</th>
                                                  <th>Country</th>
                                                  <th>Region</th>
                                                  <th>State</th>
                                                </tr>
                                              </thead>
                                               <?php 
                                                if (!empty($states)) {
                                                foreach($states as $row) { ?>
                                              <tbody>
                                                <tr>
                                                  <td><?php echo $row['state_id'];?></td>
                                                  <td><?php echo $row['country_id'];?></td>
                                                  <td><?php echo $row['region_id'];?></td>
                                                  <td><?php echo $row['state_name'];?></td>
                                                </tr>
                                              </tbody>
                                                  <?php  } } ?>  
                                            </table>
                                          </div>
                                        </div>
                                      </div>

                                    </div>
                                  </div>
                                </section> 
                            </div>

                            <!-- City Tab --> 
                            <div class="tab-pane fade" id="custom-tabs-one-settings" role="tabpanel" aria-labelledby="custom-tabs-one-settings-tab">
                                <section class="content">
                                  <div class="container-fluid">
                                    <div class="row">
                                      <!-- left column -->
                                      <div class="col-md-6">
                                        <!-- general form elements -->
                                        <div class="card card-secondary">
                                          <div class="card-header">
                                            <h3 class="card-title">City Master</h3>
                                          </div>
                                          <!-- form start -->
                                          <form action="<?php echo base_url('Location/add_city') ?>" method="POST">
                                            
                                            <div class="card-body">
                                              <div class="form-group">
                                                <label>Country</label>
                                                <select class="form-control" name="country" id="country">
                                                  <option value="">Select a Country</option>
                                                   
                                                   <?php 
                                                    if (!empty($data)) {
                                                    foreach($data as $row) { ?>
                                                      
                                                    <option value="<?php echo $row['country_id'];?>">
                                                      <?php echo $row['country_name'];?>
                                                    </option>
                                                    <?php  } } ?>
                                                   
                                                </select><br>

                                                <label>Region</label>
                                                <select class="form-control" name="regions" id="regions">
                                                  <option value="">Select a Region</option>

                                                </select><br>

                                                <label>State</label>
                                                <select class="form-control" name="state" id="state">
                                                  <option value="">Select a State</option>

                                                </select><br>

                                                <label>City</label>
                                                <input type="text" name="city_name" id="city_name" class="form-control" placeholder="City Name" required>
                                              </div>
                                            </div>
                                            <div class="card-footer">
                                              <button type="submit" class="btn btn-primary">Submit</button>
                                            </div>
                                          </form>
                                        </div>
                                        <!-- /.card -->  

                                      </div>

                                      <div class="col-md-6">
                                        <div class="card card-secondary">
                                          <div class="card-header">
                                            <h3 class="card-title">City Table</h3>
                                          </div>
                                          <div class="card-body">
                                            <table class="table table-bordered">
                                              <thead>
                                                <tr>
                                                  <th>Sr. No</th>
                                                  <th>Country</th>
                                                  <th>Region</th>
                                                  <th>State</th>
                                                  <th>City</th>
                                                </tr>
                                              </thead>
                                              <?php 
                                                if (!empty($city)) {
                                                foreach($city as $row) { ?>
                                              <tbody>
                                                <tr>
                                                  <td><?php echo $row['city_id'];?></td>
                                                  <td><?php echo $row['country_id'];?></td>
                                                  <td><?php echo $row['region_id'];?></td>
                                                  <td><?php echo $row['state_id'];?></td>
                                                  <td><?php echo $row['city_name'];?></td>
                                                </tr>
                                              </tbody>
                                                  <?php  } } ?> 
                                            </table>
                                          </div>
                                        </div>
                                      </div>

                                    </div>
                                  </div>
                                </section>
                            </div>
                          </div>
                        </div>
                        <!-- /.card -->
                      </div>
                    </div>
                  </div>

              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> 3.1.0-rc
    </div>
    <strong>Copyright &copy; 2014-2021 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="<?php echo base_url();?>plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url();?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="<?php echo base_url();?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url();?>plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="<?php echo base_url();?>plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url();?>plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?php echo base_url();?>plugins/jszip/jszip.min.js"></script>
<script src="<?php echo base_url();?>plugins/pdfmake/pdfmake.min.js"></script>
<script src="<?php echo base_url();?>plugins/pdfmake/vfs_fonts.js"></script>
<script src="<?php echo base_url();?>plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url();?>plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url();?>plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>dist/js/demo.js"></script>
<!-- Page specific script -->
<script src="<?php echo base_url();?>plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      // "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>

 <script type="text/javascript">
      $("document").ready(function () {

        // on change of countries, it select region 
        $("#countries").change(function(){
          var country_id = $("#countries").val();//$(this).val();
            //alert(country_id);
            if (country_id !="") 
            {
              $.ajax({
                url:'<?php echo base_url().'Location/get_regions' ?>',
                method:'POST',
                data: {country_id:country_id},
                success:function (data) {
                  $("#region").html(data);
                }
              });
            }
        });

        // on change of countries, it select region 
        $("#country").change(function(){
          var country_id = $("#country").val();//$(this).val();
           // alert(country_id);
            if (country_id !="") 
            {
              $.ajax({
                url:'<?php echo base_url().'Location/get_regions' ?>',
                method:'POST',
                data: {country_id:country_id},
                success:function (data) {
                  $("#regions").html(data);
                }
              });
            }
        });
        

        // on change of region, it select states 
        $("#regions").change(function(){
          var region_id = $(this).val();
          //alert(region_id);
          
          if (region_id !="") 
          {
            $.ajax({
              url:'<?php echo base_url().'Location/get_states' ?>',
              method:'POST',
              data:{region_id:region_id},
              success:function(data){
                $("#state").html(data);
              }
            });
          }
        });


      });
  </script>