<?php
class Location_model extends CI_model
{
	public function add_country($data)
	{
		if($this->db->insert('country',$data))
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}

	public function getdata()
	{
		return $this->db->get('country')->result_array();
	}

	public function add_region($formArray)
	{
		if($this->db->insert('region',$formArray))
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	public function get_region()
	{
		return $this->db->get('region')->result_array();
	}

	public function fetch_region($country_id)	// dropdown menu-fetch_region
	{
		$this->db->where('country_id', $country_id);
		$query= $this->db->get('region');

		$output = '<option value="">Select Region</option>';
		foreach ($query->result() as $row) {
			$output .='<option value="'.$row->region_id.'">'.$row->region_name.'</option>';
		}
		return $output;
	}

	public function add_state($formArray)
	{
		if($this->db->insert('states',$formArray))
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}

	public function get_states()
	{
		return $this->db->get('states')->result_array();
	}

	public function fetch_states($region_id)	// dropdown menu-fetch_states
	{
		$this->db->where('region_id', $region_id);
		$query= $this->db->get('states');

		$output = '<option value="">Select State</option>';
		foreach ($query->result() as $row) {
			$output .='<option value="'.$row->state_id.'">'.$row->state_name.'</option>';
		}
		return $output;
	}

	public function add_city($formArray)
	{
		if($this->db->insert('city',$formArray))
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}

	public function get_cities()
	{
		return $this->db->get('city')->result_array();
	}
}
?>