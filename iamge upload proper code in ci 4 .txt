						<?php
							if (!empty($photos)) {
								foreach ($photos as $photo) { ?>
							
							<div class="blog-post">
								<div class="post-media img-scale">
								<?php
								 $path = './public/assets/images/adminPhotos/'.$photo['image'];
								if ($photo['image'] !="" && file_exists($path)) {  ?>
									<img src="<?php echo base_url().'/public/assets/images/adminPhotos/'.$photo['image'] ?>" alt="">
								<?php	}	?>
								</div>
								<?php
							} }
							 ?>




<?php

namespace App\Controllers;
use App\Models\Image_Model;

class Photo_Controller extends BaseController
{
	public function index()
	{
		$model = new Image_Model();
		$photos['photos'] = $model->findAll();
		//echo "<pre>";print_r($photos); echo "</pre>"; exit();
		return view('user/photos', $photos);
	}
}