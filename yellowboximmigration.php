
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Best Immigration Consultants in Abu Dhabi | Visa service in Abu Dhabi</title>
    <meta
        content="We provide clear and practical solutions for immigration, abroad jobs, work permit, and visa if you are planning to migrate abroad. Get Advice From Professionals for your Canada, Australia UK, USA,  immigration."
        name="description">
    <meta content="" name="keywords">
    <style>
    #main {
        margin-top: 50px;
    }
     #main p{
        color:black !important;
    }
    .testimonial-item p{
        color:white !important;
    }
    
   .icon-boxes p{
        color:black !important;
    }
    .about .video-box {
        background-size: 100% 100% !important;
    }

    .clients .owl-item img {
        opacity: 1 !important;
        width: 100% !important;
        filter: none !important;
    }

    .clients .owl-item {
        padding: 20px;
    }

    .mytitle {
        font-size: 20px;
        color: #1f1e1e !important;
        font-weight: 700;
        margin: 0 0 30px 0;
    }

    .clients .owl-item img {
        width: 100%;
    }

    .clrwt {
        color: white;
    }

    .values .card-body {
        width: max-content !important;
        padding-top: 10px !important;
        padding-bottom: 0px !important;
    }

    .w-bg {
        background-color: #fff !important;
    }

    @media only screen and (max-device-width: 480px) {
        section {
            padding: 10px;
        }

        #main {
            margin-top: 40px !important;
        }

        .pb-5,
        .py-5 {
            padding-bottom: 1rem !important;
        }

        .why-us {
            margin-top: 50px !important;
        }

        .pt-5,
        .py-5 {
            padding-top: 0px;
        }

        .mysection {
            padding-top: 0rem !important;
        }

        .about .video-box {
            background-size: 100% 100%;
        }

        html,
        body {
            width: 100%;
            margin: 0px;
            padding: 0px;
            overflow-x: hidden;
        }
    }

    .bgwhite {
        background-color: white !important;
    }

    .count-title {
        font-size: 40px;
        font-weight: normal;
        margin-top: 10px;
        margin-bottom: 0;
        text-align: center;
        background-color: white
    }

    .count-text {
        font-size: 13px;
        font-weight: normal;
        margin-top: 10px;
        margin-bottom: 0;
        text-align: center;
    }

    .fa-2x {
        margin: 0 auto;
        float: none;
        display: table;
        color: #4ad1e5;
    }

    .activelink {
        background: #305E90;
        color: #fff;
    }

    .blockquote-box {
        box-shadow: 0px 10px 1px #ddd, 0 10px 20px #ccc;
        padding: 8px;
        margin-bottom: 25px;
    }

    .blockquote-box .square {
        width: 100px;
        min-height: 50px;
        margin-right: 22px;
        text-align: center !important;
        /*background-color: #46B8DA;*/
        padding: 20px 0;
    }

    h4,
    h5 {
        font-weight: bold;
    }

    .al-cta-flat {
        background: #1d1f2b;
        font-family: 'Source Sans Pro', sans-serif;
        padding: 100px 0;
    }

    .al-cta-flat .al-cta-box h2 {
        color: #fff;
        font-size: 42px;
        font-weight: 900;
    }

    .al-cta-flat .al-cta-box p {
        color: #dcdcdc;
        font-size: 22px;
    }

    .al-cta-flat .al-cta-box .btn {
        border-radius: 30px;
    }

    #cta-4 {
        background: url(assets/img/cta-4.jpg);
        padding-top: 100px;
        padding-bottom: 100px;
        background-position: center center;
    }

    #cta-4 .cta-txt {
        padding-left: 80px;
    }

    #cta-4 .cta-txt h3 {
        line-height: 1.15;
        margin-bottom: 20px;
    }

    #cta-4 .cta-txt p {
        margin-bottom: 30px;
    }

    .backclr {
        background-color: white !important;
    }

    .left-align {
        text-align: left;
    }

    .pay {
        width: 94px;
    }
    .icon-boxes p{
        color:black !important;
    }
    </style>
    <link rel="shortcut icon" href="favicon.ico?v=2" type="image/x-icon">
  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="assets/css/style.css" rel="stylesheet">
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-T2VPSBB');</script>
<!-- End Google Tag Manager -->
<!--<script>(function(w, d) { w.CollectId = "5fb4f308f8fa003ca2a4caa1"; var h = d.head || d.getElementsByTagName("head")[0]; var s = d.createElement("script"); s.setAttribute("type", "text/javascript"); s.async=true; s.setAttribute("src", "https://collectcdn.com/launcher.js"); h.appendChild(s); })(window, document);</script>-->

<!-- End Google Tag Manager (noscript) -->

    <script src="https://use.fontawesome.com/ff318a2e37.js"></script>
    <script type="application/ld+json">
    {
        "@context": "https://schema.org",
        "@type": "Corporation",
        "name": "Yellow Box Immigration",
        "alternateName": "Yellow Box Immigration",
        "url": "https://yellowboximmigration.com/",
        "logo": "https://yellowboximmigration.com/assets/img/logo-black.png",
        "sameAs": [
            "https://www.facebook.com/yellowboximmigration",
            "https://twitter.com/YellowboxI",
            "https://www.instagram.com/yellowboximmigration",
            "https://youtu.be/6j5DS-upCUU",
            "https://www.linkedin.com/company/yellow-box-immigration",
            "https://yellowboximmigration.com/"
        ]
    }
    </script>
    <script type="application/ld+json">
    {
        "@context": "https://schema.org",
        "@type": "ProfessionalService",
        "name": "Yellow Box Immigration",
        "image": "https://yellowboximmigration.com/assets/img/logo-black.png",
        "@id": "",
        "url": "https://yellowboximmigration.com/",
        "telephone": "+971 24412912",
        "priceRange": "Medium",
        "address": {
            "@type": "PostalAddress",
            "streetAddress": "Suite 801, 8th floor ABDULLAH BIN DARWEESH BUILDING 5th Street,",
            "addressLocality": "Abu Dhabi",
            "postalCode": "307501",
            "addressCountry": "AE"
        },
        "geo": {
            "@type": "GeoCoordinates",
            "latitude": 24.496642,
            "longitude": 54.373769
        },
        "openingHoursSpecification": {
            "@type": "OpeningHoursSpecification",
            "dayOfWeek": [
                "Monday",
                "Tuesday",
                "Wednesday",
                "Thursday",
                "Friday",
                "Saturday",
                "Sunday"
            ],
            "opens": "00:00",
            "closes": "23:59"
        },
        "sameAs": [
            "https://www.facebook.com/yellowboximmigration",
            "https://twitter.com/YellowboxI",
            "https://www.instagram.com/yellowboximmigration",
            "https://www.linkedin.com/company/yellow-box-immigration",
            "https://youtu.be/6j5DS-upCUU",
            "https://yellowboximmigration.com/"
        ]
    }
    </script>
</head>

<body style="overflow-x: hidden;">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T2VPSBB" height="0" width="0"
            style="display:none;visibility:hidden"></iframe></noscript>
    <!-- ======= Top Bar ======= -->
  <section id="topbar" class="d-none d-lg-block">
    <div class="container d-flex">
      <div class="contact-info mr-auto">
        <ul>
          <li><i class="icofont-envelope"></i><a href="mailto:info@yellowboximmigration.com">info@yellowboximmigration.com</a></li>
          <li><i class="icofont-phone"></i><a href="tel:+971 507 835 393">+971 507 835 393</a>, <a href="tel:+971 244 12 912">+971 244 12 912</a></li>
        </ul>
      </div>
      <div class="contact-info mr-auto">
        <ul style="background-color:white;">
          <li><img height="30" width="auto"  src="assets/american-express.png" alt="head-logo"></li>
          <li> <img height="30" width="auto" src="assets/visa-master.png" alt="footer-logo"> </li>
           <li><img height="30" width="auto" src="assets/payPal.png" alt="footer-logo"> </li> 
        </ul>
      </div>
      <div class="cta">
        <a href="#" data-toggle="modal" data-target="#exampleModal" class="scrollto">Get Started</a>
      </div>
    </div>
  </section>

  <!-- ======= Header ======= -->
  <header id="header">
    <div class="container d-flex">

      <div class="logo mr-auto">
        <h1 class="text-light"><a href="http://yellowboximmigration.com/"><img src="assets/img/logo-black.png" width="180" height="45" alt="header-logo"></a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
      </div>

      <nav class="nav-menu d-none d-lg-block">
        <ul style=" margin-left: 43px;">
          <li class="active"><a href="http://yellowboximmigration.com/">Home</a></li>
          
           <li class="drop-down"><a href="#">Skilled Immigration</a>
                <ul>
                  <li><a href="canada">Canada</a></li>
                  <li><a href="newzealand">New Zealand</a></li>
                  
                  <li><a href="australia">Australia</a></li>
                </ul>
              </li>
             <li><a href="work-permit-visa">Work Permit</a></li>
              <li class="drop-down"><a href="#">Business Visa</a>
                <ul>
                  <li><a href="business-visa-usa">USA Business Visa</a></li>
                  <li><a href="business-visa-uk">UK Business Visa</a></li>
                  <li><a href="business-visa-canada">Canada Business Visa</a></li>
                  <li><a href="business-visa-europe">Europe Business Visa</a></li>
                </ul>
              </li>
              <li><a href="study-visa">Study Visa</a></li>  
            <li><a href="about-us">About Us</a></li>
          <li><a href="contact-us">Contact Us</a></li>
        </ul>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->    <hr>
    <marquee style=" color: #9e710a; border-bottom: 1px solid rgba(255, 255, 255, 0.05);" behavior="scroll"
        direction="left">Welcome to Yellow Box Immigration services regardless of the current situation of covid-19. we
        are operating remotely our staff can serve customers over the phone and online as usual applications to the
        immigration office can be done remotely.</marquee>
    <section id="hero" class=" justify-content-center align-items-center w-bg">
        <div class="container-fliud" data-aos="fade-up">
            <div class="owl-carousel header-carousel">
                <img src="assets/img/cc.jpg" width="100%" alt="yellowbox-banner3">
                <img src="assets/img/aa.jpg" width="100%" alt="yellowbox-banner1">
                <img src="assets/img/bb.jpg" width="100%" alt="yellowbox-banner2">
            </div>
        </div>
    </section>

    <main id="main">

        <!-- ======= Why Us Section ======= -->
        <section id="why-us" class="why-us">
            <div class="container">

                <div class="row">
                    <div class="col-xl-4 col-lg-5" data-aos="fade-up">
                        <div class="content">
                            <img src="assets/img/sticker.png" width="100%">
                            <h1 style="font-size:1.5rem;">The UAE’s Most Trusted Immigration Consultant</h1>
                            <p class="text-justify">We are the leading immigration consultancy in the UAE. Our
                                immigration consultants provide clear and practical solutions for immigration, abroad
                                jobs, work permit, and visa if you are planning to migrate abroad.</p>
                            <div class="text-center">
                                <a href="#services" class="more-btn">Learn More <i class="bx bx-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-8 col-lg-7 d-flex">
                        <div class="icon-boxes d-flex flex-column justify-content-center">
                            <div class="row">
                                <div class="col-xl-4 d-flex align-items-stretch" data-aos="fade-up"
                                    data-aos-delay="100">
                                    <div class="icon-box mt-4 mt-xl-0">
                                        <i class="icofont-airplane-alt"></i><br><br>
                                        <h4>Work Permit</h4>
                                        <p class="left-align">Live, work & settle in abroad, without any restriction</p>
                                    </div>
                                </div>
                                <div class="col-xl-4 d-flex align-items-stretch" data-aos="fade-up"
                                    data-aos-delay="200">
                                    <div class="icon-box mt-4 mt-xl-0">
                                        <i class="icofont-worker"></i><br><br>
                                        <h4>Skilled Immigration</h4>
                                        <p class="left-align">Explore Excellent career opportunities with a perfect
                                            match to your skills.</p>
                                    </div>
                                </div>
                                <div class="col-xl-4 d-flex align-items-stretch" data-aos="fade-up"
                                    data-aos-delay="300">
                                    <div class="icon-box mt-4 mt-xl-0">
                                        <i class="icofont-learn"></i><br><br>
                                        <h4>Study Visa</h4>
                                        <p class="left-align">Learn with higher education from international
                                            universities</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- End Why Us Section -->

        <!-- ======= About Section ======= -->
        <section id="about" class="about section-bg">
            <div class="container">
                <div class="row">

                    <div class="col-xl-5 col-lg-6 video-box d-flex justify-content-center align-items-stretch"
                        data-aos="fade-right">
                        <a href="https://youtu.be/6j5DS-upCUU" class="venobox play-btn mb-4" data-vbtype="video"
                            data-autoplay="true"></a>
                    </div>

                    <div
                        class="col-xl-7 col-lg-6 icon-boxes d-flex flex-column align-items-stretch justify-content-center py-5 px-lg-5">
                        <h4 data-aos="fade-up">About us</h4>
                        <p data-aos="fade-up">Our team comprises of highly qualified immigration experts who will not
                            only guide you with the most suitable course of action but will ensure that your immigration
                            process goes smoothly with optimal results</p>

                        <div class="icon-box" data-aos="fade-up">
                            <div class="icon"><i class="icofont-meeting-add"></i></div>
                            <h4 class="title"><a href="">Free Consultation</a></h4>
                            <p class="description">Our Consultants have profound knowledge of the Visa Application
                                Processes in different countries.</p>
                        </div>

                        <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                            <div class="icon"><i class="icofont-teacher"></i></div>
                            <h4 class="title"><a href="">Pre-Post Landing Services</a></h4>
                            <p class="description">We provide direct one-on-one comprehensive assistance on the basic
                                needs of the guest house, accommodation.</p>
                        </div>

                        <div class="icon-box" data-aos="fade-up" data-aos-delay="200">
                            <div class="icon"><i class="icofont-certificate"></i></div>
                            <h4 class="title"><a href="">Accreditation Immigration Consultancy</a></h4>
                            <p class="description">Yellow Box Immigration is ICCRC & MARA Certified Company.</p>
                        </div>

                    </div>
                </div>

            </div>
        </section>
        <!-- End About Section -->

        <section id="cta-3" class="bg-darkblue cta-section division">
            <div class="container white-color">
                <div class="row d-flex align-items-center">
                    <div class="col-lg-12">
                        <div class="cta-txt text-center">
                            <h4 class="h4-xs txt-400">Need immigration &amp; visa consultation? &nbsp;&nbsp;
                                <button class="btn btn-warning yellow-btn" data-toggle="modal"
                                    data-target="#exampleModal">Free Consultation</button>
                            </h4>
                        </div>
                    </div>
                </div> <!-- End row -->
            </div> <!-- End container -->
        </section>

        <!-- ======= Services Section ======= -->
        <section id="services" class="services section-bg">
            <div class="container">

                <div class="section-title" data-aos="fade-up">
                    <h2>Yellow Box Immigration Services</h2>
                    <p class="text-center">Comprehensive Immigration Solution with High Success Approval Rate</p>
                </div>

                <div class="row">
                    <div class="col-lg-4 col-md-6" data-aos="fade-up">
                        <div class="icon-box">
                            <div class="icon"><i class="icofont-wallet"></i></div>
                            <h4 class="title"><a href="business-visa-canada">Business Immigration</a></h4>
                            <p class="description">With this visa, you can own and manage business abroad.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="100">
                        <div class="icon-box">
                            <div class="icon"><i class="icofont-institution"></i></div>
                            <h4 class="title"><a href="study-visa">Study Visa</a></h4>
                            <p class="description">Pursue the education of your dream of your dream.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="200">
                        <div class="icon-box">
                            <div class="icon"><i class="icofont-visa-alt"></i></div>
                            <h4 class="title"><a href="work-permit-visa">Work Permit Visa</a></h4>
                            <p class="description">Great opportunity for those who want a new job.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="300">
                        <div class="icon-box">
                            <div class="icon"><i class="icofont-spanner"></i></div>
                            <h4 class="title"><a href="canada">Skilled Immigration</a></h4>
                            <p class="description">Start your new career in abroad.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="400">
                        <div class="icon-box">
                            <div class="icon"><i class="icofont-briefcase-2"></i></div>
                            <h4 class="title"><a href="">Tourist & Visitor Visa</a></h4>
                            <p class="description">Travel around the world</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="500">
                        <div class="icon-box">
                            <div class="icon"><i class="icofont-globe"></i></div>
                            <h4 class="title"><a href="">2nd Citizenship Visa</a></h4>
                            <p class="description">Enjoy the added security 2nd citizenship visa</p>
                        </div>
                    </div>
                </div>

            </div>
        </section><!-- End Services Section -->

        <section id="about-4" class="bg-lightgrey bg-tra-city  about-section division">
            <div class="container">
                <div class="row d-flex align-items-center">


                    <!-- ABOUT TEXT -->
                    <div class="col-lg-7 col-xl-6">
                        <div class="about-4-txt mb-40">
                            <h3 class="h3-lg darkblue-color">We can help you, no matter how complicated your immigration
                                issue is. </h3>
                            <div class="box-list">
                                <p>All our agents have years of experience, with staff who have gone through the
                                    immigration process themselves, and we can give you exactly the right information
                                    you need.</p>
                                <p>We help you with all your paperwork so you can save money, and save time, without
                                    having to go through the hassle and confusion of managing paperwork yourself.</p>
                                <p>We provide FREE post landing services to help you find a house, get your health
                                    insurance, assist you in finding a job and get you settled in your new home.</p>
                                <button class="btn btn-warning" data-toggle="modal" data-target="#exampleModal">Get
                                    Consultation</button><br>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5 col-xl-5">
                        <div class="about-4-img text-center">
                            <img class="img-fluid" src="assets/img/cand.jpg" height="auto" width="100%"
                                alt="about-image">
                        </div>
                    </div>
                </div> <!-- End row -->
            </div> <!-- End container -->
        </section>
        <div class="clearfix"></div>

        <section id="about-4" class="bg-lightgrey bg-tra-city  about-section division">
            <div class="container">
                <div class="row d-flex align-items-center">

                    <div class="col-lg-5 col-xl-5">
                        <div class="about-4-img text-center">
                            <img class="img-fluid" src="assets/img/khan.jpg" width="100%" alt="about-image" style="height: 363px;
    object-fit: contain;">
                            <p class="text-center">Ahmed M. Khan – Vice President, </p>
                            <p class="text-center">South Asia and the Middle East</p>
                        </div>
                    </div>

                    <div class="col-lg-7 col-xl-6">
                        <div class="about-4-txt mb-40">
                            <h3 class="h3-lg darkblue-color">Mr. Khan holds the FINRA Series 7 and 63 licenses. </h3>
                            <div class="box-list">
                                <p>Ahmed M. Khan is CanAm Investor Services’ VP of the Middle East and South Asia. Mr.
                                    Khan practiced EB-5 law for prominent EB-5 law firms in the U.S. for over 5 years
                                    before joining CanAm, and now heads the firm’s EB-5 growing practices in the Middle
                                    East, South Asia, and Russia. Mr. Khan is stationed in Dubai and has worked with
                                    many investors based in all parts of the Middle East to invest and file their EB-5
                                    petitions.</p>
                                <p>Having filed more than 500 individual EB-5 cases as an attorney, Mr. Khan possesses
                                    extensive knowledge about the EB-5 process, Source of Funds issues, and the
                                    ever-changing US Immigration landscape. Additionally, Mr. Khan has been featured on
                                    numerous EB-5 informational panels, seminars, and other speaking engagements,
                                    including TiE Global Seminars, EB-5 Investor Magazine Conferences, and ILW Investor
                                    Seminars.</p>

                            </div>
                        </div>
                    </div>

                </div> <!-- End row -->
            </div> <!-- End container -->
        </section>
        <div class="clearfix"></div>

        <div class="container-fliud"
            style="background-image: url('assets/img/blue-map.jpg');padding-right: 25px;padding-left: 25px;">
            <div class="row">
                <br />
                <div class="col-md-12 text-center" data-aos="fade-down"><br>
                    <h2 class="clrwt">Why Choose Yellow Box Immigration?</h2><br>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-md-3 " data-aos="fade-right">
                    <div class="counter backclr"><br>
                        <i class="fa fa-users fa-2x"></i>
                        <h2 class="timer1 count-title count-number1" data-to="2000+" data-speed="1500">2000+</h2>
                        <p class="count-text ">Happy Clients</p><br>
                    </div>
                </div>
                <div class="col-md-3" data-aos="fade-up">
                    <div class="counter backclr"><br>
                        <i class="fa fa-smile-o fa-2x"></i>
                        <h2 class="timer1 count-title count-number1" data-to="100%" data-speed="1500">100%</h2>
                        <p class="count-text ">Satisfaction</p><br>
                    </div>
                </div>
                <div class="col-md-3" data-aos="fade-up">
                    <div class="counter backclr"><br>
                        <i class="fa fa-star fa-2x"></i>
                        <h2 class="timer1 count-title count-number1" id="push1" data-to="4.5" data-speed="1">4.5</h2>
                        <p class="count-text ">Reviews</p><br>
                    </div>
                </div>
                <div class="col-md-3" data-aos="fade-left">
                    <div class="counter backclr"><br>
                        <i class="fa fa-university fa-2x"></i>
                        <h2 class="timer1 count-title count-number1" data-to="180+" data-speed="1500">180+</h2>
                        <p class="count-text ">Universities</p><br>
                    </div>
                </div><br>
            </div><br>
        </div><br>
        <div class="clearfix">&nbsp;&nbsp;</div>

        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center" data-aos="fade-down">
                    <h2>Immigration Opportunities</h2><br><br>
                </div>
                <div class="col-md-6" data-aos="fade-right">
                    <div class="blockquote-box clearfix">
                        <div class="square pull-left">
                            <img src="assets/img/canada.png" class="img-rounded" height="70" />
                        </div>
                        <h5>Canada</h5>
                        <p>
                            Canada has an outstanding reputation for its diversity and economic growth. Canada has
                            created a society of mixed languages, cultures and religions.
                            <br />
                            <a href="">Read More</a>
                        </p>
                    </div>
                    <div class="blockquote-box blockquote-primary clearfix" style="">
                        <div class="square pull-left">
                            <img src="assets/img/australia.png" class="img-rounded" height="70" />

                        </div>
                        <h5>Australia</h5>
                        <p>
                            The best immigration consultancy is here for you and your family to fulfil your dreams of
                            residing, travelling, working in Australia without any hustle.<br />
                            <a href="">Read More</a>
                        </p>
                    </div>
                </div>
                <div class="col-md-6" data-aos="fade-left">
                    <div class="blockquote-box blockquote-info clearfix">
                        <div class="square pull-left">
                            <img src="assets/img/new-zealand.png" class="img-rounded" height="70" />

                        </div>
                        <h5>New Zealand</h5>
                        <p>
                            New Zealand has a lot of opportunities to study and seek employment for a bright future. New
                            Zealand welcomes all its migrants to settle down permanently.<br />
                            <a href="">Read More</a>
                        </p>
                    </div>
                    <div class="blockquote-box blockquote-warning clearfix">
                        <div class="square pull-left">
                            <img src="assets/img/united-kingdom.png" class="img-rounded" height="70" />

                        </div>
                        <h5>United Kingdom</h5>
                        <p>
                            The UK is the right country to seek career opportunities and your immigration consultant can
                            make a success of your plans to live in the UK<br />

                            <a href="" class="">Read More</a>
                        </p>
                    </div>

                </div>
                <div class="col-md-6" data-aos="fade-right">
                    <div class="blockquote-box blockquote-info clearfix">
                        <div class="square pull-left">
                            <img src="assets/img/usa.png" class="img-rounded" height="70" />

                        </div>
                        <h5>United States</h5>
                        <p>
                            The United States hosts a rich and diverse culture, which has been embraced by millions of
                            people from around the world.<br />
                            <a href="">Read More</a>
                        </p>
                    </div>

                </div>
            </div>
        </div>

        <section id="cta-4" class="bg-fixed cta-section division">
            <div class="container white-color clrwt">
                <div class="row d-flex align-items-center">
                    <div class="col-md-8 col-lg-7 offset-md-4 offset-lg-5">
                        <div class="cta-txt">
                            <h3 class="h3-xl" data-aos="fade-right" style="color:#fff">One of the best Immigration Consultants in Abu
                                Dhabi, UAE </h3>
                            <div class="btn-wrap clrwt" data-aos="fade-left">
                                <button class="btn btn-warning" data-toggle="modal" data-target="#exampleModal">Get
                                    Consultation</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        </div>
        </section>

        

<section id="clients" class="clients">
    <div class="container" data-aos="fade-up">
        <div class="section-title" data-aos="fade-up">
            <h2>Our Successful Approvals</h2>

        </div>
        <div class="owl-carousel clients-carousel">
             <img src="https://yellowboximmigration.com/approval/document/001.jpg">
             <img src="https://yellowboximmigration.com/approval/document/002.jpg">
             <img src="https://yellowboximmigration.com/approval/document/003.jpg">
             <img src="https://yellowboximmigration.com/approval/document/004.jpg">
             <img src="https://yellowboximmigration.com/approval/document/005.jpg">
             <img src="https://yellowboximmigration.com/approval/document/006.jpg">
             <img src="https://yellowboximmigration.com/approval/document/007.jpg">
             <img src="https://yellowboximmigration.com/approval/document/008.jpg">
             <img src="https://yellowboximmigration.com/approval/document/009.jpg">
             <img src="https://yellowboximmigration.com/approval/document/010.jpg">
             <img src="https://yellowboximmigration.com/approval/document/011.jpg">
             <img src="https://yellowboximmigration.com/approval/document/012.jpg">
             <img src="https://yellowboximmigration.com/approval/document/013.jpg">
            
           
             
    </div>
</section>
 <style>
   .testimonial-item p{
         color:#fff !important;
     }
 </style>
        <!-- ======= Testimonials Section ======= -->
        <section id="testimonials" class="testimonials">
            <div class="container" data-aos="fade-up">
                <div class="section-title " data-aos="fade-up">
                    <h2 class="w-t">What Our Clients Say</h2>

                </div>
                <div class="owl-carousel testimonials-carousel">

                    <div class="testimonial-item">
                        <h3>Richelyn Basadre</h3>
                        <p style="color:white !important">
                            <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                            I felt so lucky to be part of YELLOW BOX IMMIGRATION. They provide an ultimate opportunity to me as a job seeker. They are very professional, especially to Ms. Joanna Marie, who guided and assisted my Sweden application. We are happy to choose the Yellowbox immigration.
                            <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                        </p>
                    </div>
                    
                    <div class="testimonial-item">
                        <h3>Rynette Holgado</h3>
                        <p style="color:white !important">
                            <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                            I am very thankful to be assisted by Ms. Joanna Marie Regarding my application for Permanent Residence in Canada. Yellowbox is very lucky to have such an employee like her. I hope this will acknowledge by the management.
                            <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                        </p>
                    </div>
                    <div class="testimonial-item">
                        <h3>Johirul Haque</h3>
                        <p style="color:white !important">
                            <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                            Yellowbox Immigration Agency was very professional from the start of my application. They were patient, explained every realistic option, without any false hopes, and completed all my documentation as efficiently as possible. In particular, MsJoanna was very supportive and was available to chat at all times whenever. I highly recommend anyone looking to immigrate to work with them
                            <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                        </p>
                    </div>
                    
                    <div class="testimonial-item">
                        <h3>Michelle Mayo</h3>
                        <p style="color:white !important">
                            <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                            We met their consultant Joanna Marie and she's been great. I really admire her dedication to customer satisfaction. With the yellow box, they make us feel important especially Joanna Marie. Joanna Marie is so professional and respectful and very accomodating and she gives all her effort to provide the best service possible. thanks to the yellow box and joanna we have our hopes high and full again.
                            <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                        </p>
                    </div>
                    
                    <div class="testimonial-item">
                        <h3>Kenneth James Larce</h3>
                        <p style="color:white !important">
                            <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                            3 major reasons why I chose Yellowbox Immigration? Professionalism, Transparency & Commitment.  Thanks to you Danjay and Joana! You guys are so great and really commendable for your expertise in your field of work most especially in handling all my inquiries. I am a hundred percent confident that I am in the right place and right people towards my dream to migrate to Canada. It was a wonderful experience to meet this professional team of Yellowbox. Thank you so much, Joana, Danjay, and to the Yellowbox Management!
                            <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                        </p>
                    </div>
                    
                    <div class="testimonial-item">
                        <h3>OM Lama</h3>
                        <p style="color:white !important">
                            <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                            Enough satisfied to select YellowBox Immigration for my immigration process. One of the best
                            services anyone can get. I met Mr. Arif he is very friendly and supportive and always
                            available when you have any queries related to your immigration. I think YellowBox
                            Immigration is the best immigration consultants in Abu Dhabi.
                            <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                        </p>
                    </div>

                    <div class="testimonial-item">
                        <h3>Keshu Khalil</h3>
                        <p style="color:white !important">
                            <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                            We are happy to choose Yellow box immigration for our immigration procedure. I would highly
                            recommend Yellow box immigration who are looking for immigration services."Best immigration
                            consultancy in Abu Dhabi. "Best immigration consultant for Canada PR
                            <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                        </p>
                    </div>

                    <div class="testimonial-item">
                        <h3>jay Rana</h3>
                        <p style="color:white !important">
                            <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                            I'm happy to choose yellow box immigration for my immigration procedure I would highly
                            recommend yellow box immigration who are looking for immigration services.mr.. Arif had
                            detail information about the procedure. highly recommend thank you
                            <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                        </p>
                    </div>

                    <div class="testimonial-item">
                        <h3>Fahad PC</h3>
                        <p style="color:white !important">
                            <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                            I highly recommend their service. They are very professional and have really good service.
                            They returned all my calls promptly and completed my application quickly. Very fair prices
                            for all the extensive work they do. Highly recommend. Special Thanks to you all for your
                            kind and quick actions. I can say finally I found fair and true people like you.
                            <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                        </p>
                    </div>

                    <div class="testimonial-item">
                        <h3>Manikandan P</h3>
                        <p style="color:white !important">
                            <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                            Very professional and helpful staff. They guided and assisted me at each and every step from
                            scratch throughout the process. I strongly recommend her as I had such a wonderful
                            experience dealing with, We are happy to choose Yellow box immigrations for our immigration
                            procedure. I would highly recommend Yellow box immigrations who are looking for immigration
                            services.
                            <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                        </p>
                    </div>
                    <div class="testimonial-item">
                        <h3>Aviraj Ganesh</h3>
                        <p style="color:white !important">
                            <i class="bx bxs-quote-alt-left quote-icon-left"></i>

                            Excellent service provided by yellow box immigration for Polland work permit! I met Mr Arif,
                            given excellent information!

                            <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                        </p>
                    </div>

                    <div class="testimonial-item">
                        <h3>Keshu Khalil</h3>
                        <p style="color:white !important">
                            <i class="bx bxs-quote-alt-left quote-icon-left"></i>


                            We are happy to choose Yellow box immigration for our immigration procedure. I would highly
                            recommend Yellow box immigration who are looking for immigration services."Best immigration
                            consultancy in Abu Dhabi. "Best immigration consultant for Canada PR".


                            <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                        </p>
                    </div>

                    <div class="testimonial-item">
                        <h3>Ram Kumar Bista</h3>
                        <p style="color:white !important">
                            <i class="bx bxs-quote-alt-left quote-icon-left"></i>


                            Very much satisfied to select Yellow Box Immigration for my immigration process. One of the
                            best services anyone can get. Staff are very friendly and supportive and always available
                            when you have any queries related to your immigration. I think Yellow Box Immigration is the
                            best immigration consultants in Abu Dhabi for Canada and Europe work permit and investment
                            programs.


                            <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                        </p>
                    </div>
                    
                    <div class="testimonial-item">
                        <h3>Majdi Wassouf</h3>
                        <p style="color:white !important">
                            <i class="bx bxs-quote-alt-left quote-icon-left"></i>


                            Yellow Box Immigration consultation agency deserves to be a brand in the immigration business. Their professionalism, honesty, explain every small detail no matter what it’s makes the really hassle-free process, I do appreciate the staff who’s handling my file especially Joanna and the rest of the team, they’re doing a great job to make my dream become true, thank you YellowBox agency


                            <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                        </p>
                    </div>

                    <div class="testimonial-item">
                        <h3>Rhea Repuyan</h3>
                        <p style="color:white !important">
                            <i class="bx bxs-quote-alt-left quote-icon-left"></i>


                           I'm glad I've found Yellow Box Immigration for my desire to migrate to other countries. I've talked to Alex, one of their consultants. She explained to me very well the process from the very beginning until receiving the visa. I highly recommend this company.


                            <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                        </p>
                    </div>
                    
                </div>

            </div>
        </section><!-- End Testimonials Section -->
        <!-- ======= Contact Section ======= -->
        <section id="contact" class="contact">
            <div class="container">
                <h2 class="text-center" data-aos="fade-up">Contact Us</h2>

                <div class="row justify-content-center" data-aos="fade-up" data-aos-delay="300">
                    <div class="col-xl-12 col-lg-12 mt-4">
                        <form action="mail.php" method="post" role="form" class="php-email-form">
                            <div class="form-row">
                                <div class="col-md-6 form-group">
                                    <input type="text" name="name" class="form-control" id="name"
                                        placeholder="Your Name" data-rule="minlen:4"
                                        data-msg="Please enter at least 4 chars" />
                                    <div class="validate"></div>
                                </div>
                                <div class="col-md-6 form-group">
                                    <input type="email" class="form-control" name="email" id="email"
                                        placeholder="Your Email" data-rule="email"
                                        data-msg="Please enter a valid email" />
                                    <div class="validate"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="text" name="phone" class="form-control" id="mobile"
                                    placeholder="Your Mobile" data-rule="minlen:10"
                                    data-msg="Please enter at least 10 digits" />
                                <div class="validate"></div>
                            </div>
                            <div class="form-group">
                                <select id="inlineFormCustomSelect2" name="reason" class="custom-select subject valid"
                                    required="" aria-invalid="false">
                                    <option value="">Your Question About..</option>
                                    <option>Student Visa</option>
                                    <option>Travel visa</option>
                                    <option>Working Visa</option>
                                    <option>Business Visa</option>
                                    <option>Visitor Visa</option>
                                    <option>Other...</option>
                                    <div class="validate"></div>
                                </select>
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" name="message" rows="5" data-rule="required"
                                    data-msg="Please write something for us" placeholder="Message"></textarea>
                                <div class="validate"></div>
                            </div>
                            <div class="mb-3">
                                <div class="loading">Loading</div>
                                <div class="error-message"></div>
                                <div class="sent-message">Your message has been sent. Thank you!</div>
                            </div>
                            <div class="text-center"><button type="submit">Send Message</button></div>
                        </form>
                    </div>

                </div>

            </div>
        </section><!-- End Contact Section -->

        <section id="faq" class="faq section-bg">
            <div class="container">

                <div class="section-title">
                    <h2 data-aos="fade-up">F.A.Q</h2>
                    <!--<p data-aos="fade-up">Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid-->
                    <!--    fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui-->
                    <!--    impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>-->
                </div>

                <div class="faq-list">
                    <ul>
                        <li data-aos="fade-up">
                            <i class="bx bx-help-circle icon-help"> </i> <a data-toggle="collapse" class="collapse"
                                href="#faq-list-1"> What types of services do you provide? <i
                                    class="bx bx-chevron-down icon-show"></i><i
                                    class="bx bx-chevron-up icon-close"></i></a>
                            <div id="faq-list-1" class="collapse show" data-parent=".faq-list">
                                <p>
                                    Our immigration consultants provide clear and practical solutions for immigration,
                                    abroad jobs, work permit, and visa if you are planning to migrate abroad. </p>

                                <a href="https://yellowboximmigration.com/business-visa-canada">
                                    <p>• Business Immigration </p>
                                </a>
                                <a href="https://yellowboximmigration.com/study-visa">
                                    <p>• Study Visa </p>
                                </a>
                                <a href="https://yellowboximmigration.com/work-permit-visa">
                                    <p>• Work Permit Visa </p>
                                </a>
                                <a href="https://yellowboximmigration.com/canada">
                                    <p>• Skilled Immigration </p>
                                </a>
                                <a href="#">
                                    <p>• Tourist & Visitor Visa </p>
                                </a>
                                <a href="#">
                                    <p>• 2nd Citizenship Visa

                                    </p>
                                </a>
                            </div>
                        </li>

                        <li data-aos="fade-up" data-aos-delay="100">
                            <i class="bx bx-help-circle icon-help"> </i> <a data-toggle="collapse" href="#faq-list-2"
                                class="collapsed"> How long does it take to process an immigration application? <i
                                    class="bx bx-chevron-down icon-show"></i><i
                                    class="bx bx-chevron-up icon-close"></i></a>
                            <div id="faq-list-2" class="collapse" data-parent=".faq-list">
                                <p>
                                    The processing time is different in each case – depending on the type of
                                    application, the program, and the number of applications currently being processed.
                                    Get in touch with our team of immigration consultants for more information.

                                </p>
                            </div>
                        </li>

                        <li data-aos="fade-up" data-aos-delay="200">
                            <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" href="#faq-list-3"
                                class="collapsed">Can immigration Authorities reject my application? <i
                                    class="bx bx-chevron-down icon-show"></i><i
                                    class="bx bx-chevron-up icon-close"></i></a>
                            <div id="faq-list-3" class="collapse" data-parent=".faq-list">
                                <p>Yes, Some country immigration regulations are strict. Applications may be delayed due
                                    to technical errors or rejected due to incorrect and/or insufficient supporting
                                    documents.
                                </p>
                            </div>
                        </li>

                        <li data-aos="fade-up" data-aos-delay="300">
                            <i class="bx bx-help-circle icon-help"> </i> <a data-toggle="collapse" href="#faq-list-4"
                                class="collapsed"> Who can migrate?
                                <i class="bx bx-chevron-down icon-show"></i><i
                                    class="bx bx-chevron-up icon-close"></i></a>
                            <div id="faq-list-4" class="collapse" data-parent=".faq-list">
                                <p>
                                    • Who are skilled in their occupation with few years of experience </p>

                                <p>• Possessing a bachelor’s degree or higher qualification. </p>

                                <p>• Applicants having an offer of employment from an employer in the migrating country
                                    will be preferred.

                                </p>
                            </div>
                        </li>

                        <li data-aos="fade-up" data-aos-delay="400">
                            <i class="bx bx-help-circle icon-help"> </i> <a data-toggle="collapse" href="#faq-list-5"
                                class="collapsed"> What documents do I need? <i
                                    class="bx bx-chevron-down icon-show"></i><i
                                    class="bx bx-chevron-up icon-close"></i></a>
                            <div id="faq-list-5" class="collapse" data-parent=".faq-list">
                                <p>
                                    • Education </p>
                                <p>• Experience </p>
                                <p>• Language </p>
                                <p>• Police clearance </p>
                                <p>• Medical Clearance

                                </p>
                            </div>
                        </li>

                        <li data-aos="fade-up" data-aos-delay="400">
                            <i class="bx bx-help-circle icon-help"> </i> <a data-toggle="collapse" href="#faq-list-6"
                                class="collapsed"> Is there a visa interview? <i
                                    class="bx bx-chevron-down icon-show"></i><i
                                    class="bx bx-chevron-up icon-close"></i></a>
                            <div id="faq-list-6" class="collapse" data-parent=".faq-list">
                                <p>
                                    It completely depends upon the visa type you are applying for and the country you
                                    want to migrate to whether the intentions of the applicant are genuine.

                                </p>
                            </div>
                        </li>

                        <li data-aos="fade-up" data-aos-delay="400">
                            <i class="bx bx-help-circle icon-help"> </i> <a data-toggle="collapse" href="#faq-list-7"
                                class="collapsed"> Do you offer post landing services? <i
                                    class="bx bx-chevron-down icon-show"></i><i
                                    class="bx bx-chevron-up icon-close"></i></a>
                            <div id="faq-list-7" class="collapse" data-parent=".faq-list">
                                <p>
                                    Post landing services are facilities, offered to you once you land in your
                                    destination country. Ask for references of people who availed the post landing
                                    services and check the quality of the services.

                                </p>
                            </div>
                        </li>

                        <li data-aos="fade-up" data-aos-delay="400">
                            <i class="bx bx-help-circle icon-help"> </i> <a data-toggle="collapse" href="#faq-list-8"
                                class="collapsed"> How are you different from others?
                                <i class="bx bx-chevron-down icon-show"></i><i
                                    class="bx bx-chevron-up icon-close"></i></a>
                            <div id="faq-list-8" class="collapse" data-parent=".faq-list">
                                <p>
                                    Yellow Box Immigration is ICCRC & MARA Certified Company. Our Consultants have
                                    profound knowledge of the Visa Application Processes in different countries.

                                </p>
                            </div>
                        </li>

                    </ul>
                </div>

            </div>
        </section>

    </main><!-- End #main -->

    <!-- ======= Footer ======= -->
<style>
  .white-color{
      color:#fff !important;
  }
    h5{
      color:orange !important;  
    }
</style>
<footer id="footer" style="background-image: url(assets/img/map.jpg);color: white;" data-aos="fade-up">
  <br><br><br>
  <div class="container">
    <!-- FOOTER CONTENT -->
    <div class="row"> 
      <!-- FOOTER INFO -->
      <div class="col-xl-4">
        <div class="footer-info mb-40">

          <!-- Footer Logo -->
          <!-- For Retina Ready displays take a image with double the amount of pixels that your image will be displayed (e.g 420 x 100 pixels) -->
          <img src="assets/img/logo-blackf.png" width="210" height="50" alt="footer-logo">

          <!-- Text --> 
          <p class="p-sm mt-25 white-color">We are an eminent team of professionals with advanced knowledge in immigration, relocation and overseas establishment With a dedicated, experienced, and passionate team located in UAE.
          </p>

        </div>  
      </div>


      <!-- FOOTER CONTACTS -->
      <div class="col-lg-4 col-xl-4">
        <div class="footer-box mb-40">
          <h5 class="h5-md ">Contact</h5>
          <p class="p-sm white-color" >121 King Street, Melbourne, Victoria 3000 Australia</p>
          <p class="white-color">Phone:<a href="tel:+971 507 835 393" class="clrwt"> +971 507 835 393</a></p>
          <p class="white-color">Phone:<a href="tel:+971 244 12 912" class="clrwt"> +971 244 12 912</a></p>
          <p class="p-sm foo-email white-color">Email: <a href="mailto:info@yellowboximmigration.com" class="clrwt">info@yellowboximmigration.com</a></p>
        </div>
      </div>
      <div class="col-lg-4 col-xl-4">
        <div class="footer-box mb-40">

          <!-- Title -->
          <h5 class="h5-md">Head Office</h5>

          <p class="p-sm mt-20 white-color">
          Address (Abu Dhabi)</p>

          <!-- Address -->
          <p class="p-sm white-color">Suite 801, 8th floor ABDULLAH BIN DARWEESH BUILDING 5th Street,</p> 
          <p class="p-sm white-color"> Shk. Zayed Bin Sultan St, Near Al Masraf Bank Abu Dhabi United Arab Emirates</p>

          <!-- Phone -->
          <p class="white-color">Phone: <a href="tel:+971507835393" class="clrwt">+971507835393
</a></p>

          <!-- Email -->
          <p class="p-sm foo-email white-color">Email: <a href="mailto:info@yellowboximmigration.com" class="clrwt">info@yellowboximmigration.com</a></p>


        </div>
      </div>


      <!-- FOOTER LINKS -->

    </div>   <!-- END FOOTER CONTENT -->
    <!-- FOOTER CONTENT -->
    <br>
    <div class="row"> 

      <!-- FOOTER CONTACTS -->
      <div class="col-lg-4 col-xl-4">
       <!-- <div class="footer-box mb-40">

          Title 
          <h5 class="h5-md">Branch Office India</h5>-->
          <!-- Address 
          <p class="p-sm">Suite no 3 & 4,Nabeel Plaza, </p>   
          <p class="p-sm">Near Himayat Baugh,Aurangabad
          431001, India</p>-->

          <!-- Phone 
          <p>Phone: <a href="tel:+91-9850632449" class="clrwt">+91-9850632449</a></p>-->

          <!-- Email 
          <p class="p-sm foo-email">Email: <a href="mailto:info@yellowboximmigration.com" class="clrwt">info@yellowboximmigration.com</a></p>
        </div>-->
        
        
      </div>


      <!-- FOOTER LINKS -->
      <div class="col-lg-4 col-xl-4">
        <div class="footer-box mb-40">

          <!-- Title -->
          <h5 class="h5-md">Branch Office Australia</h5>


          <!-- Address -->
          <p class="p-sm white-color">5 Lilliana Blvd Carrum</p>  
          <p class="p-sm white-color">downs Melbourne,VIC 3201</p>

          <!-- Phone -->
          <p class="white-color">Phone: <a href="tel:+61-405007844" class="clrwt">+61-405007844</a></p>

          <!-- Email -->
          <p class="p-sm foo-email white-color">Email: <a href="mailto:info@yellowboximmigration.com" class="clrwt">info@yellowboximmigration.com</a></p>
          
        </div>
        
      </div>
      <div class="col-lg-4 col-xl-4">

        <div class="footer-links mb-40">

          <!-- Title -->
          <h5 class="h5-md">Useful Links</h5>

          <!-- Footer Links -->
          <ul class="foo-links clearfix">
            <li><a href="https://yellowboximmigration.com/" class="clrwt">Home</a></li>
            <li><a href="about-us" class="clrwt">About Us</a></li>
            <li><a href="how-it-work" class="clrwt">How It Works</a></li>  
            <!--<li><a href="#" class="clrwt">Success Stories</a></li>-->
            <li><a href="contact-us" class="clrwt">Contact</a></li>
            <li><a href="https://yellowboximmigration.com/blog" class="clrwt">Blog</a></li>
          </ul>
 <div class="social-links text-center text-lg-right pt-3 pt-lg-0">
     
      <img class="img img-fluid pay" src="assets/american-express.png"  alt="footer-logo">
      <img class="img img-fluid pay" src="assets/visa-master.png" alt="footer-logo"> 
     <img class="img img-fluid pay" src="assets/payPal.png" alt="footer-logo"> 
        </div>
        </div>

      </div>
      
      <div class="container d-lg-flex py-4">

        <div class="mr-lg-auto text-center text-lg-left">
          <div class="copyright">
            &copy; Copyright <strong><span>YellowBox</span></strong>. All Rights Reserved
          </div>
          <img src="assets/4.7.jpg" class="img img-responsive img-fluid" style="    max-width: 39%;">
        </div>
       
        <div class="social-links text-center text-lg-right pt-3 pt-lg-0">
          <a href="https://www.facebook.com/yellowboximmigration" class="facebook"><i class="bx bxl-facebook"></i></a>
          <a href="https://twitter.com/YellowboxI" class="twitter"><i class="bx bxl-twitter"></i></a>
          <a href="https://www.instagram.com/yellowboximmigration/" class="instagram"><i class="bx bxl-instagram"></i></a>
          <a href="https://www.linkedin.com/company/yellow-box-immigration" class="linkedin"><i class="bx bxl-linkedin"></i></a>
        </div>
      </div>
    </div>

  </div>     <!-- End container -->                   
</footer> <!-- END FOOTER-1 -->

  <div class="container-fluid mfooter" style="display: none;">
    <div class="row">
      <div class="col text-center">
        <button class="btn btn-warning bntbottom form-control"><a href="tel:+971 507 835 393" class="clrwt">Call Now</a></button>
      </div>
      <div class="col text-center">
        <button class="btn btn-warning bntbottom form-control"><a class="clrwt" data-toggle="modal" data-target="#exampleModal">Enquire Now</a></button>
      </div>
    </div>
  </div>
<style>
  @media only screen and (max-device-width: 480px) {
    #chat-bot-launcher-container.chat-bot-avatar-launcher #chat-bot-launcher {
      padding-bottom: 40px;
    }
  .mfooter{
    position: fixed;
      width: 100%;
      bottom: 0;
      left: 0;
      z-index: 999;
      display: block ! important;
      background: #0000006b;
    }
    .social-links{
      margin-bottom: 20px;
    }
    .bntbottom{
      box-shadow: 5px 5px 5px #ccc;
    }
}
</style>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header border-bottom-0">
        <h5 class="modal-title text-center" id="exampleModalLabel">Free Consultation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <form id="crm-form" onsubmit="return crm();" action="mail.php" method="post">
        <div class="modal-body">
         
          <div class="form-group">
            <label for="password1" class="active">Name</label>
            <input type="text" class="form-control" id="name" name="name" required="" placeholder="Name">
          </div>
           <div class="form-group">
            <label for="email1" class="active">Email Address</label>
            <input type="email" name="email" class="form-control" required="" id="email_id" aria-describedby="emailHelp" placeholder="Enter email">
            </div>
          <div class="form-group">
            <label for="password1" class="active">Phone No</label>
            <input type="text" class="form-control" id="phone_number" required="" name="phone" placeholder="Phone No">
          </div>
          <div class="form-group">
            <label for="password1">Visa For</label>
            <select id="inlineFormCustomSelect2" name="visa" class="custom-select visa" required="">
                                  <option value="">Visa For</option>  
                                  <option>Australia</option>
                                  <option>Canada</option>
                                  <option>United Kingdom</option>
                                  <option>USA</option>
                                  <option>New Zealand</option>
                                  
                              </select>
          </div>
          
        </div>
       <!--  <div class=" col-md-12 form-btn">
          <button type="submit" class="btn btn-primary tra-white-hover submit">Submit</button>
        </div> -->
        <div class="col-md-12 form-btn modal-footer border-top-0 d-flex justify-content-center">  
                            <button type="submit" class="btn btn-primary tra-white-hover yellow-btn submit">Send Request</button> 
                          </div>
      </form>
    </div>
  </div>
</div>

<!-- Vendor JS Files -->
<script src="assets/vendor/jquery/jquery.min.js"></script>
<script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
<script src="assets/vendor/php-email-form/validate.js"></script>
<script src="assets/vendor/jquery-sticky/jquery.sticky.js"></script>
<script src="assets/vendor/venobox/venobox.min.js"></script>
<script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
<script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
<script src="assets/vendor/aos/aos.js"></script>

<!-- Template Main JS File -->
<script src="assets/js/main.js"></script>


    <script src="https://www.gstatic.com/firebasejs/5.0.3/firebase-app.js">
    	</script>
    	<script src="https://www.gstatic.com/firebasejs/5.0.3/firebase-firestore.js">
    	</script>
	<script src="sales/crm.js"></script>    <script>
    $(document).ready(function() {
        setTimeout(function() {
            // $('#exampleModal').modal('show');
        }, 10000);
    });
    </script>
</body>

</html>